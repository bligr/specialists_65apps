package com.bligr.specialists.model.tools;

import com.bligr.specialists.model.data_manager.entity.Specialty;
import com.bligr.specialists.model.data_manager.entity.Worker;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Igor on 29.09.2017.
 */
class SpecialtiesGrouper implements ISpecialtiesGrouper {

    @Override
    public List<Specialty> groupWorkers(List<Worker> workers) {
        List<Specialty> specialties = new LinkedList<>();
        //проходимся по всем рабочим
        for (Worker worker : workers) {
            List<Specialty> workerSpecialties = worker.getSpecialties();
            //игнорируем null-списки
            if (workerSpecialties == null)
                break;
            //проходимся по всем специальностям рабочего
            for (Specialty workerSpecialty : workerSpecialties) {
                Specialty listWorkerSpeciality;
                //ищем в списке эквивалентную специальность
                int index = specialties.indexOf(workerSpecialty);
                //если нашли
                if (index != -1) {
                    //запоминаем найденную специальность
                    listWorkerSpeciality = specialties.get(index);
                } else {
                    //добавляем новую специальность
                    specialties.add(workerSpecialty);
                    //запоминаем специальность рабочего
                    listWorkerSpeciality = workerSpecialty;
                }
                //добавляем в специальность рабочего
                listWorkerSpeciality.addWorker(worker);
            }
        }
        return specialties;
    }
}
