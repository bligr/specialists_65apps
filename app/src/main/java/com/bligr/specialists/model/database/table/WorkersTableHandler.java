package com.bligr.specialists.model.database.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.bligr.specialists.model.database.DatabaseConstants;
import com.bligr.specialists.model.database.DatabaseException;
import com.bligr.specialists.model.database.entity.SpecialtyDb;
import com.bligr.specialists.model.database.entity.WorkerDb;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Igor on 04.10.2017.
 */
public class WorkersTableHandler extends SQLiteTableHandler<WorkerDb> {

    private static final String GET_ALL_QUERY = "SELECT * " +
            "FROM " +
            "\"" + DatabaseConstants.TABLE_WORKERS + "\" w, " +
            "\"" + DatabaseConstants.TABLE_WORKERS_SPECIALTIES + "\" ws, " +
            "\"" + DatabaseConstants.TABLE_SPECIALTIES + "\" s " +
            "WHERE " +
            "w." + DatabaseConstants.COLUMN_WORKER_ID + "=" +
            "ws." + DatabaseConstants.COLUMN_WORKER_ID +
            " AND " +
            "s." + DatabaseConstants.COLUMN_SPECIALTY_ID + "=" +
            "ws." + DatabaseConstants.COLUMN_SPECIALTY_ID;

    private final SpecialtiesTableHandler specialtiesTableHandler;
    private final WorkersSpecialtiesTableHandler workersSpecialtiesTable;

    WorkersTableHandler(SQLiteOpenHelper databaseProvider, ISQLiteTableHelper sqLiteTableHelper,
                        SpecialtiesTableHandler specialtiesTableHandler,
                        WorkersSpecialtiesTableHandler workersSpecialtiesTable) {
        super(databaseProvider, sqLiteTableHelper, DatabaseConstants.TABLE_WORKERS);
        this.specialtiesTableHandler = specialtiesTableHandler;
        this.workersSpecialtiesTable = workersSpecialtiesTable;
    }

    @NonNull
    @Override
    ContentValues toContentValues(WorkerDb databaseEntity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseConstants.COLUMN_WORKER_ID, databaseEntity.getId());
        contentValues.put(DatabaseConstants.COLUMN_FIRST_NAME, databaseEntity.getFirstName());
        contentValues.put(DatabaseConstants.COLUMN_LAST_NAME, databaseEntity.getLastName());
        contentValues.put(DatabaseConstants.COLUMN_BIRTHDAY, databaseEntity.getBirthday());
        contentValues.put(DatabaseConstants.COLUMN_AVATAR_URL, databaseEntity.getAvatarUrl());
        return contentValues;
    }

    @NonNull
    @Override
    WorkerDb toDatabaseEntity(Cursor cursor) {
        WorkerDb workerDb = new WorkerDb();
        int idIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_WORKER_ID);
        int firstNameIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_FIRST_NAME);
        int lastNameIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_LAST_NAME);
        int birthdayIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_BIRTHDAY);
        int avatarUrlIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_AVATAR_URL);

        workerDb.setId(cursor.getInt(idIndex));
        workerDb.setFirstName(cursor.getString(firstNameIndex));
        workerDb.setLastName(cursor.getString(lastNameIndex));
        workerDb.setBirthday(cursor.getString(birthdayIndex));
        workerDb.setAvatarUrl(cursor.getString(avatarUrlIndex));
        return workerDb;
    }

    @NonNull
    @Override
    String[] getIdColumns() {
        return new String[]{
                DatabaseConstants.COLUMN_FIRST_NAME,
                DatabaseConstants.COLUMN_LAST_NAME,
                DatabaseConstants.COLUMN_BIRTHDAY
        };
    }

    @Override
    public void updateAll(List<WorkerDb> workers) {
        tableHelper.doInTransaction(getWritableDatabase(), sqLiteDatabase -> {
            //очищаем все таблицы
            this.deleteAll(sqLiteDatabase);
            workersSpecialtiesTable.deleteAll(sqLiteDatabase);
            specialtiesTableHandler.deleteAll(sqLiteDatabase);

            //исключаем возможность значения null в идентификаторах рабочих
            updateIds(workers);
            //обновляем всех работников
            super.updateAll(sqLiteDatabase, workers);

            //обновляем специальности каждого рабочего, создавая записи в связующей таблице
            for (WorkerDb workerDb : workers) {
                List<SpecialtyDb> specialtyDbs = workerDb.getSpecialties();
                if (specialtyDbs != null) {
                    for (SpecialtyDb specialtyDb : specialtyDbs) {
                        //добавляем запись в связующую таблицу
                        workersSpecialtiesTable.saveWorkerSpecialty(sqLiteDatabase, workerDb, specialtyDb);
                        //обновляем специальности
                        specialtiesTableHandler.updateByPrimaryKey(sqLiteDatabase, specialtyDb);
                    }
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public List<WorkerDb> superGetAll() {
        return super.getAll();
    }

    @NonNull
    @Override
    public List<WorkerDb> getAll() {
        Cursor cursor = getReadableDatabase().rawQuery(GET_ALL_QUERY, null);
        //создаем список из "сырых" пар "рабочий-специальность"
        List<WorkerDb> workerDbs = tableHelper.cursorToList(cursor,
                cursor1 -> {
                    WorkerDb workerDb = toDatabaseEntity(cursor1);
                    SpecialtyDb specialtyDb = specialtiesTableHandler.toDatabaseEntity(cursor1);
                    List<SpecialtyDb> specialties = new LinkedList<>();
                    specialties.add(specialtyDb);
                    workerDb.setSpecialties(specialties);
                    return workerDb;
                });

        //проверяем, есть ли записи в базе данных
        if (workerDbs.isEmpty())
            throw new DatabaseException("There is no data of workers in database!");

        //группируем специальности рабочих
        groupSpecialties(workerDbs);

        cursor.close();
        return workerDbs;
    }

    /**
     * Группировка специальностей по рабочим (удаление дубликатов рабочих).
     * @param workerDbs "Сырой" список рабочих.
     */
    private void groupSpecialties(List<WorkerDb> workerDbs) {
        for (int i = 0; i < workerDbs.size() - 1; i++) {
            for (int j = i + 1; j < workerDbs.size(); j++) {
                WorkerDb workerDb1 = workerDbs.get(i);
                WorkerDb workerDb2 = workerDbs.get(j);
                if (workerDb1.equals(workerDb2)) {
                    //noinspection ConstantConditions
                    SpecialtyDb specialtyDb = workerDb2.getSpecialties().get(0);
                    //noinspection ConstantConditions
                    workerDb1.getSpecialties().add(specialtyDb);
                    workerDbs.remove(j);
                    break;
                }
            }
        }
    }

    /**
     * Установка случайных идентификаторов рабочим, у которых они не заданы.
     * @param workers Список рабочих.
     */
    private void updateIds(List<WorkerDb> workers) {
        for (WorkerDb worker : workers) {
            if (worker.getId() == null)
                worker.setId(tableHelper.getRandomId());
        }
    }
}
