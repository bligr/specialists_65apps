package com.bligr.specialists.model.database;

import android.content.Context;

import com.bligr.specialists.model.database.entity.WorkerDb;
import com.bligr.specialists.model.database.table.SpecialtiesTableHandler;
import com.bligr.specialists.model.database.table.WorkersSpecialtiesTableHandler;
import com.bligr.specialists.model.database.table.WorkersTableHandler;

import java.util.List;

import rx.Observable;

/**
 * Created by Igor on 27.09.2017.
 */
class DatabaseConnector implements IDatabaseConnector {

    private final Context context;
    private final WorkersTableHandler workersTableHandler;
    private final SpecialtiesTableHandler specialtiesTableHandler;
    private final WorkersSpecialtiesTableHandler workersSpecialtiesTableHandler;

    DatabaseConnector(Context context, WorkersTableHandler workersTableHandler,
                      SpecialtiesTableHandler specialtiesTableHandler,
                      WorkersSpecialtiesTableHandler workersSpecialtiesTableHandler) {
        this.context = context;
        this.workersTableHandler = workersTableHandler;
        this.specialtiesTableHandler = specialtiesTableHandler;
        this.workersSpecialtiesTableHandler = workersSpecialtiesTableHandler;
    }

    @Override
    public Observable<List<WorkerDb>> getWorkers() {
        return Observable.defer(() -> {
            List<WorkerDb> workers = workersTableHandler.getAll();
            return Observable.just(workers);
        });
    }

    @Override
    public Observable<Void> saveWorkers(List<WorkerDb> workers) {
        return Observable.<Void>defer(() -> {
            workersTableHandler.updateAll(workers);
            return Observable.just(null);
        })/*.doOnNext(o -> {
            Log.d("TAG1", "workers: " + workersTableHandler.superGetAll());
            Log.d("TAG1", "workers-specialties: " + workersSpecialtiesTableHandler.getAll());
            Log.d("TAG1", "specialties: " + specialtiesTableHandler.getAll());
        })*/;
    }
}
