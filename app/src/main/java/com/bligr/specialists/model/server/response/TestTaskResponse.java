package com.bligr.specialists.model.server.response;

import com.bligr.specialists.model.server.entity.WorkerJson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Igor on 28.09.2017.
 */
public class TestTaskResponse {

    @SerializedName("response")
    @Expose
    private List<WorkerJson> workers;

    public List<WorkerJson> getWorkers() {
        return workers;
    }

    public void setWorkers(List<WorkerJson> workers) {
        this.workers = workers;
    }
}
