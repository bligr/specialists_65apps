package com.bligr.specialists.model.tools.worker_info_handlers;

import android.content.Context;
import android.content.res.Resources;

import com.bligr.specialists.R;

import java.text.ParseException;

/**
 * Created by Igor on 09.10.2017.
 */
class AgesHandler implements IAgesHandler {

    private final Context context;
    private final IAgeCalculator ageCalculator;

    AgesHandler(Context context, IAgeCalculator ageCalculator) {
        this.context = context;
        this.ageCalculator = ageCalculator;
    }

    @Override
    public String convertAgeToText(String birthday) {
        String workerAgeStr;
        try {
            //указываем возраст
            workerAgeStr = getWorkerAgeAsString(birthday);
        } catch (ParseException | NullPointerException e) {
            //указываем плэйсхолдер
            workerAgeStr = context.getString(R.string.no_data_placeholder);
        }
        return workerAgeStr;
    }

    private String getWorkerAgeAsString(String birthday)
            throws ParseException, NullPointerException {
        //если null, не выполняем дальше
        if (birthday == null)
            throw new NullPointerException();

        //создание строки с возрастом
        int workerAge = ageCalculator.calculate(birthday);
        Resources resources = context.getResources();
        String workerAgePlural = resources.getQuantityString(R.plurals.ages, workerAge);
        return context.getString(R.string.worker_age_placeholder, workerAge, workerAgePlural);
    }
}
