package com.bligr.specialists.model.database.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.bligr.specialists.model.database.entity.IDatabaseEntity;

import java.util.LinkedList;
import java.util.List;

import rx.functions.Func1;

/**
 * Created by Igor on 04.10.2017.
 */
abstract class SQLiteTableHandler<D extends IDatabaseEntity> {

    @NonNull
    final SQLiteOpenHelper databaseProvider;
    @NonNull
    final ISQLiteTableHelper tableHelper;
    @NonNull
    final String tableName;

    SQLiteTableHandler(@NonNull SQLiteOpenHelper databaseProvider,
                       @NonNull ISQLiteTableHelper tableHelper, @NonNull String tableName) {
        this.databaseProvider = databaseProvider;
        this.tableHelper = tableHelper;
        this.tableName = tableName;
    }

    @NonNull
    abstract ContentValues toContentValues(D databaseEntity);

    @NonNull
    abstract D toDatabaseEntity(Cursor cursor);

    @NonNull
    abstract String[] getIdColumns();

    @NonNull
    public List<D> getAll() {
        Func1<SQLiteDatabase, Cursor> getCursorFunction = sqLiteDatabase ->
                sqLiteDatabase.query(tableName, null, null, null, null, null, null);

        //преобразуем в список
        Func1<Cursor, List<D>> usingCursorFunction = cursor ->
                tableHelper.cursorToList(cursor, this::toDatabaseEntity);

        return tableHelper.getByUsingCursor(getReadableDatabase(), getCursorFunction,
                usingCursorFunction);
    }

    public D getBy(List<Pair<String, String>> columnValues) {
        Pair<String, String[]> andSelection = tableHelper.createAndSelection(columnValues);

        Func1<SQLiteDatabase, Cursor> getCursorFunction = sqLiteDatabase -> sqLiteDatabase
                .query(tableName, null, andSelection.first, andSelection.second, null, null, null);

        return tableHelper.getByUsingCursor(getReadableDatabase(), getCursorFunction,
                this::toDatabaseEntity);
    }

    public D getBy(String column, String value) {
        List<Pair<String, String>> columnValues = tableHelper.createColumnValues(column, value);
        return getBy(columnValues);
    }

    public void updateAll(List<D> list) {
        updateAll(getWritableDatabase(), list);
    }

    void updateAll(SQLiteDatabase writableDatabase, List<D> list) {
        for (D databaseEntity : list) {
            updateByPrimaryKey(writableDatabase, databaseEntity);
        }
    }

    void updateOrInsert(ContentValues contentValues, String whereClause, String[] whereArgs) {
        updateOrInsert(getWritableDatabase(), contentValues, whereClause, whereArgs);
    }

    void updateOrInsert(SQLiteDatabase writableDatabase, ContentValues contentValues,
                        String whereClause, String[] whereArgs) {
        int update = writableDatabase.update(tableName, contentValues, whereClause, whereArgs);
        if (update == 0)
            writableDatabase.insert(tableName, null, contentValues);
    }

    void insert(ContentValues contentValues) {
        insert(getWritableDatabase(), contentValues);
    }

    void insert(SQLiteDatabase writableDatabase, ContentValues contentValues) {
        writableDatabase.insert(tableName, null, contentValues);
    }

    void updateByPrimaryKey(D databaseEntity) {
        updateByPrimaryKey(getWritableDatabase(), databaseEntity);
    }

    void updateByPrimaryKey(SQLiteDatabase writableDatabase, D databaseEntity) {
        ContentValues contentValues = toContentValues(databaseEntity);
        //используем первичный ключ
        String[] selectionColumns = getIdColumns();
        //создаем выборку
        LinkedList<Pair<String, String>> columnValues =
                tableHelper.createColumnValues(contentValues, selectionColumns);
        Pair<String, String[]> selection = tableHelper.createAndSelection(columnValues);
        //обновляем очередную запись
        updateOrInsert(writableDatabase, contentValues, selection.first, selection.second);
    }

    public void deleteAll() {
        deleteAll(getWritableDatabase());
    }

    void deleteAll(SQLiteDatabase writableDatabase) {
        writableDatabase.delete(tableName, null, null);
    }

    @SuppressWarnings("WeakerAccess")
    SQLiteDatabase getReadableDatabase() {
        return databaseProvider.getReadableDatabase();
    }

    @SuppressWarnings("WeakerAccess")
    SQLiteDatabase getWritableDatabase() {
        return databaseProvider.getWritableDatabase();
    }
}
