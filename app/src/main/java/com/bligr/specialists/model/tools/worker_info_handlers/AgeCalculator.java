package com.bligr.specialists.model.tools.worker_info_handlers;

import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by Igor on 02.10.2017.
 */
class AgeCalculator implements IAgeCalculator {

    private static final Locale LOCALE = Locale.getDefault();
    private static final SimpleDateFormat[] DATE_FORMATS = {
            new SimpleDateFormat("dd-MM-yyyy", LOCALE),
            new SimpleDateFormat("yyyy-MM-dd", LOCALE),
    };

    AgeCalculator() {
    }

    @Override
    public int calculate(@NonNull Date date) {
        //календарь для текущего времени
        Calendar calendarNow = Calendar.getInstance();
        //календарь для указанного времени
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        //считаем разницу лет
        int age = calendarNow.get(Calendar.YEAR) - calendar.get(Calendar.YEAR);
        //учитываем разницу дней
        if (calendarNow.get(Calendar.DAY_OF_YEAR) < calendar.get(Calendar.DAY_OF_YEAR))
            age--;
        return age;
    }

    @Override
    public int calculate(@NonNull String dateStr) throws ParseException {
        for (SimpleDateFormat dateFormat : DATE_FORMATS) {
            try {
                dateFormat.setLenient(false);
                Date date = dateFormat.parse(dateStr);
                return calculate(date);
            } catch (ParseException ignored) {
            }
        }
        throw new ParseException("Unsupported date format: " + dateStr, 0);
    }
}
