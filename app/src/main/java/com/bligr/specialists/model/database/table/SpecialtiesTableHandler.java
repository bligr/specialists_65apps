package com.bligr.specialists.model.database.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.bligr.specialists.model.database.DatabaseConstants;
import com.bligr.specialists.model.database.entity.SpecialtyDb;

/**
 * Created by Igor on 04.10.2017.
 */
public class SpecialtiesTableHandler extends SQLiteTableHandler<SpecialtyDb> {

    SpecialtiesTableHandler(SQLiteOpenHelper databaseProvider, ISQLiteTableHelper sqLiteTableHelper) {
        super(databaseProvider, sqLiteTableHelper, DatabaseConstants.TABLE_SPECIALTIES);
    }

    @NonNull
    @Override
    ContentValues toContentValues(SpecialtyDb databaseEntity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseConstants.COLUMN_SPECIALTY_ID, databaseEntity.getId());
        contentValues.put(DatabaseConstants.COLUMN_NAME, databaseEntity.getName());
        return contentValues;
    }

    @NonNull
    @Override
    SpecialtyDb toDatabaseEntity(Cursor cursor) {
        SpecialtyDb specialtyDb = new SpecialtyDb();
        int idIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_SPECIALTY_ID);
        int nameIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_NAME);

        specialtyDb.setId(cursor.getInt(idIndex));
        specialtyDb.setName(cursor.getString(nameIndex));
        return specialtyDb;
    }

    @NonNull
    @Override
    String[] getIdColumns() {
        return new String[] {
                DatabaseConstants.COLUMN_SPECIALTY_ID
        };
    }
}
