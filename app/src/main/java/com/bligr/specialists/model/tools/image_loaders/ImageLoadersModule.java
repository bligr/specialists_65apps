package com.bligr.specialists.model.tools.image_loaders;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Igor on 27.09.2017.
 */
@Module
public class ImageLoadersModule {

    @Provides
    @Singleton
    IImageLoader provideImageLoader(Context context) {
        return new GlideImageLoader(context);
    }
}
