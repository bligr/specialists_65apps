package com.bligr.specialists.model.data_manager;

import com.bligr.specialists.model.data_manager.entity.Specialty;

import java.util.List;

import rx.Observable;

/**
 * Created by Igor on 23.09.2017.
 */
public interface IDataManager {
    Observable<List<Specialty>> loadSpecialities();
}
