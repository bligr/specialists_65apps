package com.bligr.specialists.model.tools.worker_info_handlers;

import android.content.Context;
import android.support.annotation.Nullable;

import com.bligr.specialists.R;
import com.bligr.specialists.model.data_manager.entity.Worker;

/**
 * Created by Igor on 09.10.2017.
 */
class NamesHandler implements INamesHandler {

    private final Context context;

    NamesHandler(Context context) {
        this.context = context;
    }

    @Nullable
    @Override
    public String asName(@Nullable String rawName) {
        if (rawName == null)
            return null;
        return rawName.substring(0, 1).toUpperCase() + rawName.substring(1).toLowerCase();
    }

    @Nullable
    @Override
    public String toFullName(Worker worker) {
        return toFullName(worker.getFirstName(), worker.getLastName());
    }

    @Nullable
    @Override
    public String toFullName(String firstName, String lastName) {
        if (firstName == null || lastName == null)
            return null;
        return context.getString(R.string.worker_name_placeholder, firstName, lastName);
    }
}
