package com.bligr.specialists.model.tools;

import com.bligr.specialists.model.data_manager.entity.Specialty;
import com.bligr.specialists.model.data_manager.entity.Worker;

import java.util.List;

import rx.Observable;

/**
 * Created by Igor on 01.10.2017.
 */
public interface ISpecialtiesKeeper {

    Observable<List<Specialty>> getAllSpecialties();

    Observable<List<Worker>> getWorkers(int specialtyId);
}
