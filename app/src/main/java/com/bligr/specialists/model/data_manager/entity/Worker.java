package com.bligr.specialists.model.data_manager.entity;

import android.support.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Igor on 23.09.2017.
 */
public class Worker implements IAppEntity, Serializable {

    private String firstName;
    private String lastName;
    private String birthday;
    private String avatarUrl;
    @Nullable
    private List<Specialty> specialties;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @Nullable
    public List<Specialty> getSpecialties() {
        return specialties;
    }

    public void setSpecialties(@Nullable List<Specialty> specialties) {
        this.specialties = specialties;
    }
}
