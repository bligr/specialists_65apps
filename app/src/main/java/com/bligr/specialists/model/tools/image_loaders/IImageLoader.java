package com.bligr.specialists.model.tools.image_loaders;

import android.widget.ImageView;

/**
 * Created by Igor on 01.10.2017.
 */
public interface IImageLoader {
    void loadImageUrl(String url, ImageView imageView);
}
