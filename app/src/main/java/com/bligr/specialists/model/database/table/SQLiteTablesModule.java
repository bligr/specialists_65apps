package com.bligr.specialists.model.database.table;

import android.database.sqlite.SQLiteOpenHelper;

import java.util.Random;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Igor on 04.10.2017.
 */
@Module
public class SQLiteTablesModule {

    @Provides
    @Singleton
    ISQLiteTableHelper provideSQLiteTablesHelper(Random random) {
        return new SQLiteTableHelper(random);
    }

    @Provides
    @Singleton
    Random provideRandom() {
        return new Random();
    }

    @Provides
    @Singleton
    WorkersTableHandler provideWorkersTable(SQLiteOpenHelper databaseProvider,
                                            ISQLiteTableHelper sqLiteTableHelper,
                                            SpecialtiesTableHandler specialtiesTable,
                                            WorkersSpecialtiesTableHandler workersSpecialtiesTable) {
        return new WorkersTableHandler(databaseProvider, sqLiteTableHelper, specialtiesTable,
                workersSpecialtiesTable);
    }

    @Provides
    @Singleton
    SpecialtiesTableHandler provideSpecialtiesTable(SQLiteOpenHelper databaseProvider,
                                                    ISQLiteTableHelper sqLiteTableHelper) {
        return new SpecialtiesTableHandler(databaseProvider, sqLiteTableHelper);
    }

    @Provides
    @Singleton
    WorkersSpecialtiesTableHandler provideWorkersSpecialtiesTable(SQLiteOpenHelper databaseProvider,
                                                                  ISQLiteTableHelper sqLiteTableHelper) {
        return new WorkersSpecialtiesTableHandler(databaseProvider, sqLiteTableHelper);
    }
}
