package com.bligr.specialists.model.server.service;

import com.bligr.specialists.model.server.response.TestTaskResponse;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Igor on 28.09.2017.
 */
public interface ITestTaskService {

    @GET("65gb/static/raw/master/testTask.json")
    Observable<TestTaskResponse> getWorkers();
}
