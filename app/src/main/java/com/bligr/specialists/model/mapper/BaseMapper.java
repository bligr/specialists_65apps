package com.bligr.specialists.model.mapper;

import android.support.annotation.Nullable;

import com.bligr.specialists.model.data_manager.entity.IAppEntity;
import com.bligr.specialists.model.database.entity.IDatabaseEntity;
import com.bligr.specialists.model.server.entity.IJsonEntity;

import java.util.LinkedList;
import java.util.List;

import rx.functions.Func1;

/**
 * Created by Igor on 28.09.2017.
 */
abstract class BaseMapper<A extends IAppEntity, J extends IJsonEntity, D extends IDatabaseEntity>
        implements IMapper<A, J, D> {

    @Nullable
    @Override
    public List<A> toAppListFromJsonList(@Nullable List<J> jsonList) {
        return transformList(jsonList, this::toAppEntity);
    }

    @Nullable
    @Override
    public List<A> toAppListFromDatabaseList(@Nullable List<D> databaseList) {
        return transformList(databaseList, this::toAppEntity);
    }

    @Nullable
    @Override
    public List<D> toDatabaseListFromAppList(@Nullable List<A> appList) {
        return transformList(appList, this::toDatabaseEntity);
    }

    @Nullable
    private <F, S> List<S> transformList(@Nullable List<F> firstList, Func1<F, S> entityTransformer) {
        //если на входе null, возвращаем null
        if (firstList == null)
            return null;
        //иначе преобразуем список
        List<S> secondList = new LinkedList<>();
        for (F f : firstList) {
            secondList.add(entityTransformer.call(f));
        }
        return secondList;
    }
}
