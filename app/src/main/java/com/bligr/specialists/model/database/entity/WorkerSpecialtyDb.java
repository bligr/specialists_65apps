package com.bligr.specialists.model.database.entity;

/**
 * Created by Igor on 28.09.2017.
 */
public class WorkerSpecialtyDb implements IDatabaseEntity {

    private int id = -1;
    private int workerId;
    private int specialtyId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWorkerId() {
        return workerId;
    }

    public void setWorkerId(int workerId) {
        this.workerId = workerId;
    }

    public int getSpecialtyId() {
        return specialtyId;
    }

    public void setSpecialtyId(int specialtyId) {
        this.specialtyId = specialtyId;
    }

    @Override
    public String toString() {
        return "WorkerSpecialtyDb{" +
                "id=" + id +
                ", workerId=" + workerId +
                ", specialtyId=" + specialtyId +
                '}';
    }
}
