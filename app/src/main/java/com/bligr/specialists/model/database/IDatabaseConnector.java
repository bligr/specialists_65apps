package com.bligr.specialists.model.database;

import com.bligr.specialists.model.database.entity.WorkerDb;

import java.util.List;

import rx.Observable;

/**
 * Created by Igor on 27.09.2017.
 */
public interface IDatabaseConnector {
    Observable<List<WorkerDb>> getWorkers();

    Observable<Void> saveWorkers(List<WorkerDb> workers);
}
