package com.bligr.specialists.model.database.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by Igor on 05.10.2017.
 */
class SQLiteTableHelper implements ISQLiteTableHelper {

    private static final String EQUALS_QUESTION = "=?";
    private static final String IS_NULL = " is null";
    private static final String AND = " AND ";
    private final Random random;

    SQLiteTableHelper(Random random) {
        this.random = random;
    }

    @NonNull
    @Override
    public <T> T getByUsingCursor(@NonNull SQLiteDatabase sqLiteDatabase,
                                  @NonNull Func1<SQLiteDatabase, Cursor> getCursorFunction,
                                  @NonNull Func1<Cursor, T> usingCursorFunction) {
        //получаем курсор
        Cursor cursor = getCursorFunction.call(sqLiteDatabase);
        cursor.moveToFirst();
        //производим укзанные действия и получаем некоторый объект
        T t = usingCursorFunction.call(cursor);
        //закрываем курсор
        cursor.close();
        return t;
    }

    @NonNull
    @Override
    public <T> List<T> cursorToList(Cursor cursor, Func1<Cursor, T> convertToListItemFunction) {
        //встаем в начало
        cursor.moveToFirst();
        //преобразуем в список
        List<T> list = new LinkedList<>();
        while (!cursor.isAfterLast()) {
            T databaseEntity = convertToListItemFunction.call(cursor);
            list.add(databaseEntity);
            cursor.moveToNext();
        }
        return list;
    }

    @NonNull
    @Override
    public Pair<String, String[]> createAndSelection(@Nullable List<Pair<String, String>> columnValues) {
        String selection = null;
        List<String> selectionArgs = null;
        //проверяем на заполненность списка
        if (columnValues != null && !columnValues.isEmpty()) {
            int columnValuesSize = columnValues.size();
            selection = "";
            selectionArgs = new LinkedList<>();

            for (int i = 0; i < columnValuesSize; i++) {
                Pair<String, String> columnValue = columnValues.get(i);
                String column = columnValue.first;
                String value = columnValue.second;

                if (value != null) {
                    selection += column + EQUALS_QUESTION;
                    selectionArgs.add(value);
                } else {
                    selection += column + IS_NULL;
                }

                if (i < columnValuesSize - 1) {
                    selection += AND;
                }
            }
        }

        String[] argsArray = selectionArgs == null ? null :
                selectionArgs.toArray(new String[selectionArgs.size()]);
        return new Pair<>(selection, argsArray);
    }

    @NonNull
    @Override
    public LinkedList<Pair<String, String>> createColumnValues(@NonNull ContentValues contentValues,
                                                               @NonNull String[] selectionColumns) {
        LinkedList<Pair<String, String>> pairs = new LinkedList<>();
        for (String selectionColumn : selectionColumns) {
            Object value = contentValues.get(selectionColumn);
            pairs.add(new Pair<>(selectionColumn, String.valueOf(value)));
        }
        return pairs;
    }

    @NonNull
    @Override
    public List<Pair<String, String>> createColumnValues(String column, String value) {
        Pair<String, String> pair = new Pair<>(column, value);
        return Collections.singletonList(pair);
    }

    @Override
    public void doInTransaction(@NonNull SQLiteDatabase database,
                                @NonNull Action1<SQLiteDatabase> databaseAction) {
        database.beginTransaction();
        databaseAction.call(database);
        database.setTransactionSuccessful();
        database.endTransaction();
    }

    @Override
    public int getRandomId() {
        return random.nextInt();
    }
}
