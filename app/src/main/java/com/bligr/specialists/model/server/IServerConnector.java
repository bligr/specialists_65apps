package com.bligr.specialists.model.server;

import com.bligr.specialists.model.server.entity.WorkerJson;

import java.util.List;

import rx.Observable;

/**
 * Created by Igor on 27.09.2017.
 */
public interface IServerConnector {
    Observable<List<WorkerJson>> getWorkers();
}
