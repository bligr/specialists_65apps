package com.bligr.specialists.model.data_manager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Igor on 27.09.2017.
 */
@Module
public class DataManagerModule {

    @Provides
    @Singleton
    IDataManager provideDataManager() {
        return new DataManager();
    }
}
