package com.bligr.specialists.model.data_manager;

import com.bligr.specialists.app.DaggerComponents;
import com.bligr.specialists.model.data_manager.entity.Specialty;
import com.bligr.specialists.model.database.IDatabaseConnector;
import com.bligr.specialists.model.mapper.WorkersMapper;
import com.bligr.specialists.model.server.IServerConnector;
import com.bligr.specialists.model.tools.ISpecialtiesGrouper;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Igor on 23.09.2017.
 */
class DataManager implements IDataManager {

    //Connectors
    @Inject IDatabaseConnector databaseConnector;
    @Inject IServerConnector serverConnector;

    //Mappers
//    @Inject SpecialtyMapper specialtyMapper;
    @Inject WorkersMapper workersMapper;

    //Tools
    @Inject ISpecialtiesGrouper specialtiesGrouper;

    DataManager() {
        DaggerComponents.getDataManagerComponent().inject(this);
    }

    @Override
    public Observable<List<Specialty>> loadSpecialities() {
        //в первую очередь пытаемся получить данные с сервера
        return runOnIoThread(serverConnector.getWorkers()
                //преобразуя их в данные приложения
                .map(workersMapper::toAppListFromJsonList)
                //сохраняем полученные данные в базе
                .doOnNext(workers -> databaseConnector
                        .saveWorkers(workersMapper.toDatabaseListFromAppList(workers))
                        .subscribe(aVoid -> {}, Throwable::printStackTrace))
                //если не получилось, получаем из базы данных
                .onErrorResumeNext(throwable -> databaseConnector.getWorkers()
                        //преобразуя их в данные приложения
                        .map(workersMapper::toAppListFromDatabaseList))
                //группируем рабочих
                .map(specialtiesGrouper::groupWorkers));
    }

    private <T> Observable<T> runOnIoThread(Observable<T> observable) {
        return observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
