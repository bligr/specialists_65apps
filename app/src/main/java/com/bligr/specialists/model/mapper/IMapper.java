package com.bligr.specialists.model.mapper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bligr.specialists.model.data_manager.entity.IAppEntity;
import com.bligr.specialists.model.database.entity.IDatabaseEntity;
import com.bligr.specialists.model.server.entity.IJsonEntity;

import java.util.List;

/**
 * Created by Igor on 28.09.2017.
 */
interface IMapper<A extends IAppEntity, J extends IJsonEntity, D extends IDatabaseEntity> {

    @NonNull
    A toAppEntity(J jsonEntity);

    @NonNull
    A toAppEntity(D databaseEntity);

    @NonNull
    D toDatabaseEntity(A appEntity);

    @Nullable
    List<A> toAppListFromJsonList(@Nullable List<J> jsonList);

    @Nullable
    List<A> toAppListFromDatabaseList(@Nullable List<D> databaseList);

    @Nullable
    List<D> toDatabaseListFromAppList(@Nullable List<A> appList);
}
