package com.bligr.specialists.model.tools.image_loaders;

import android.content.Context;
import android.widget.ImageView;

import com.bligr.specialists.R;

/**
 * Created by Igor on 01.10.2017.
 */
class GlideImageLoader implements IImageLoader {

    private final Context context;

    GlideImageLoader(Context context) {
        this.context = context;
    }

    @Override
    public void loadImageUrl(String url, ImageView imageView) {
        GlideApp.with(context)
                .load(url)
                .placeholder(R.drawable.ic_person_black_24dp)
                .into(imageView);
    }

}
