package com.bligr.specialists.model.mapper;

import android.support.annotation.NonNull;

import com.bligr.specialists.model.data_manager.entity.Specialty;
import com.bligr.specialists.model.data_manager.entity.Worker;
import com.bligr.specialists.model.database.entity.SpecialtyDb;
import com.bligr.specialists.model.database.entity.WorkerDb;
import com.bligr.specialists.model.server.entity.WorkerJson;
import com.bligr.specialists.model.tools.worker_info_handlers.INamesHandler;

import java.util.List;

/**
 * Created by Igor on 28.09.2017.
 */
public class WorkersMapper extends BaseMapper<Worker, WorkerJson, WorkerDb> {

    @NonNull
    private final SpecialtyMapper specialtyMapper;
    @NonNull
    private final INamesHandler namesHandler;

    WorkersMapper(@NonNull SpecialtyMapper specialtyMapper, @NonNull INamesHandler namesHandler) {
        this.specialtyMapper = specialtyMapper;
        this.namesHandler = namesHandler;
    }

    @NonNull
    @Override
    public Worker toAppEntity(WorkerJson jsonEntity) {
        Worker worker = new Worker();
        String firstName = namesHandler.asName(jsonEntity.getFirstName());
        worker.setFirstName(firstName);
        String lastName = namesHandler.asName(jsonEntity.getLastName());
        worker.setLastName(lastName);
        worker.setAvatarUrl(jsonEntity.getAvatarUrl());
        worker.setBirthday(jsonEntity.getBirthday());
        List<Specialty> specialties = specialtyMapper
                .toAppListFromJsonList(jsonEntity.getSpecialties());
        worker.setSpecialties(specialties);
        return worker;
    }

    @NonNull
    @Override
    public Worker toAppEntity(WorkerDb databaseEntity) {
        Worker worker = new Worker();
        worker.setFirstName(databaseEntity.getFirstName());
        worker.setLastName(databaseEntity.getLastName());
        worker.setAvatarUrl(databaseEntity.getAvatarUrl());
        worker.setBirthday(databaseEntity.getBirthday());
        List<Specialty> specialties = specialtyMapper
                .toAppListFromDatabaseList(databaseEntity.getSpecialties());
        worker.setSpecialties(specialties);
        return worker;
    }

    @NonNull
    @Override
    public WorkerDb toDatabaseEntity(Worker appEntity) {
        WorkerDb workerDb = new WorkerDb();
        workerDb.setFirstName(appEntity.getFirstName());
        workerDb.setLastName(appEntity.getLastName());
        workerDb.setAvatarUrl(appEntity.getAvatarUrl());
        workerDb.setBirthday(appEntity.getBirthday());
        List<SpecialtyDb> specialties = specialtyMapper
                .toDatabaseListFromAppList(appEntity.getSpecialties());
        workerDb.setSpecialties(specialties);
        return workerDb;
    }
}
