package com.bligr.specialists.model.server;

import com.bligr.specialists.model.server.entity.WorkerJson;
import com.bligr.specialists.model.server.response.TestTaskResponse;
import com.bligr.specialists.model.server.service.ITestTaskService;

import java.util.List;

import rx.Observable;

/**
 * Created by Igor on 27.09.2017.
 */
class ServerConnector implements IServerConnector {

    private final ITestTaskService testTaskService;

    ServerConnector(ITestTaskService testTaskService) {
        this.testTaskService = testTaskService;
    }

    @Override
    public Observable<List<WorkerJson>> getWorkers() {
        return testTaskService.getWorkers()
                .map(TestTaskResponse::getWorkers);
    }
}
