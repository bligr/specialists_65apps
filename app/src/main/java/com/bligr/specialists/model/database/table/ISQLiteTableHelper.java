package com.bligr.specialists.model.database.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;

import java.util.LinkedList;
import java.util.List;

import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by Igor on 08.10.2017.
 */
interface ISQLiteTableHelper {

    @NonNull
    <T> T getByUsingCursor(@NonNull SQLiteDatabase sqLiteDatabase,
                           @NonNull Func1<SQLiteDatabase, Cursor> getCursorFunction,
                           @NonNull Func1<Cursor, T> usingCursorFunction);

    @NonNull
    <T> List<T> cursorToList(Cursor cursor, Func1<Cursor, T> convertToListItemFunction);

    @NonNull
    Pair<String, String[]> createAndSelection(@Nullable List<Pair<String, String>> columnValues);

    @NonNull
    LinkedList<Pair<String, String>> createColumnValues(@NonNull ContentValues contentValues,
                                                        @NonNull String[] selectionColumns);

    @NonNull
    List<Pair<String, String>> createColumnValues(String column, String value);

    void doInTransaction(@NonNull SQLiteDatabase database,
                         @NonNull Action1<SQLiteDatabase> databaseAction);

    int getRandomId();
}
