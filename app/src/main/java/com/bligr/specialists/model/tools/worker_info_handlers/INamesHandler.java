package com.bligr.specialists.model.tools.worker_info_handlers;

import android.support.annotation.Nullable;

import com.bligr.specialists.model.data_manager.entity.Worker;

/**
 * Created by Igor on 09.10.2017.
 */
public interface INamesHandler {

    /**
     * Преобразование имени в нужный формат.
     * @param rawName Черновой вариант имени.
     * @return Исправленный вариант имени.
     */
    @Nullable
    String asName(@Nullable String rawName);

    /**
     * Выделение полного имени рабочего.
     * @param worker Информация о рабочем.
     * @return Полное имя рабочего.
     */
    @Nullable
    String toFullName(Worker worker);

    /**
     * Выделение полного имени.
     * @param firstName Имя.
     * @param lastName Фамилия.
     * @return Полное имя
     */
    @Nullable
    String toFullName(String firstName, String lastName);
}
