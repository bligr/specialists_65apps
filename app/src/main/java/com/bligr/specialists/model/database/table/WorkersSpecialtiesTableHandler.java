package com.bligr.specialists.model.database.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.bligr.specialists.model.database.DatabaseConstants;
import com.bligr.specialists.model.database.entity.SpecialtyDb;
import com.bligr.specialists.model.database.entity.WorkerDb;
import com.bligr.specialists.model.database.entity.WorkerSpecialtyDb;

/**
 * Created by Igor on 04.10.2017.
 */
public class WorkersSpecialtiesTableHandler extends SQLiteTableHandler<WorkerSpecialtyDb> {

    WorkersSpecialtiesTableHandler(SQLiteOpenHelper databaseProvider,
                                   ISQLiteTableHelper sqLiteTableHelper) {
        super(databaseProvider, sqLiteTableHelper, DatabaseConstants.TABLE_WORKERS_SPECIALTIES);
    }

    @NonNull
    @Override
    ContentValues toContentValues(WorkerSpecialtyDb databaseEntity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseConstants.COLUMN_ID, databaseEntity.getId());
        contentValues.put(DatabaseConstants.COLUMN_WORKER_ID, databaseEntity.getWorkerId());
        contentValues.put(DatabaseConstants.COLUMN_SPECIALTY_ID, databaseEntity.getSpecialtyId());
        return contentValues;
    }

    @NonNull
    @Override
    WorkerSpecialtyDb toDatabaseEntity(Cursor cursor) {
        WorkerSpecialtyDb workerSpecialtyDb = new WorkerSpecialtyDb();
        int idIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_ID);
        int workerIdIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_WORKER_ID);
        int specialtyIdIndex = cursor.getColumnIndex(DatabaseConstants.COLUMN_SPECIALTY_ID);

        workerSpecialtyDb.setId(cursor.getInt(idIndex));
        workerSpecialtyDb.setWorkerId(cursor.getInt(workerIdIndex));
        workerSpecialtyDb.setSpecialtyId(cursor.getInt(specialtyIdIndex));
        return workerSpecialtyDb;
    }

    @NonNull
    @Override
    String[] getIdColumns() {
        return new String[]{
                DatabaseConstants.COLUMN_WORKER_ID
        };
    }

    void saveWorkerSpecialty(SQLiteDatabase sqLiteDatabase, WorkerDb workerDb,
                             SpecialtyDb specialtyDb) {
        Integer workerId = workerDb.getId();
        int specialtyId = specialtyDb.getId();

        WorkerSpecialtyDb workerSpecialtyDb = new WorkerSpecialtyDb();
        workerSpecialtyDb.setWorkerId(workerId);
        workerSpecialtyDb.setSpecialtyId(specialtyId);

        ContentValues contentValues = toContentValues(workerSpecialtyDb);
        //этот параметр автоинкрементируемый, в данном случае не должен указываться явно
        contentValues.remove(DatabaseConstants.COLUMN_ID);

        insert(sqLiteDatabase, contentValues);
    }
}
