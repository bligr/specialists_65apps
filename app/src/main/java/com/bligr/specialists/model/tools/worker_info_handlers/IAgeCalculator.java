package com.bligr.specialists.model.tools.worker_info_handlers;

import android.support.annotation.NonNull;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by Igor on 02.10.2017.
 */
public interface IAgeCalculator {
    int calculate(@NonNull Date date);

    int calculate(@NonNull String dateStr) throws ParseException;
}
