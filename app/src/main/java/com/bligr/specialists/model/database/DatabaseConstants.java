package com.bligr.specialists.model.database;

/**
 * Created by Igor on 04.10.2017.
 */
public class DatabaseConstants {

    public static final String TABLE_WORKERS = "Workers";
    public static final String COLUMN_WORKER_ID = "worker_id";
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_LAST_NAME = "last_name";
    public static final String COLUMN_BIRTHDAY = "birthday";
    public static final String COLUMN_AVATAR_URL = "avatar_url";

    public static final String TABLE_SPECIALTIES = "Specialties";
    public static final String COLUMN_SPECIALTY_ID = "specialty_id";
    public static final String COLUMN_NAME = "name";

    public static final String TABLE_WORKERS_SPECIALTIES = "WorkersSpecialties";
    public static final String COLUMN_ID = "id";

}
