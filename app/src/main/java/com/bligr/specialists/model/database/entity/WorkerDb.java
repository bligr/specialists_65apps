package com.bligr.specialists.model.database.entity;

import android.support.annotation.Nullable;

import java.util.List;

/**
 * Created by Igor on 28.09.2017.
 */
public class WorkerDb implements IDatabaseEntity {

    private Integer id;
    private String firstName;
    private String lastName;
    private String birthday;
    private String avatarUrl;
    @Nullable
    private List<SpecialtyDb> specialties;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @Nullable
    public List<SpecialtyDb> getSpecialties() {
        return specialties;
    }

    public void setSpecialties(@Nullable List<SpecialtyDb> specialties) {
        this.specialties = specialties;
    }

    /**
     * Сравнивание текущего объекта с передаваемым параметром.
     * @param obj Объект для сравнения.
     * @return True, если у обоих объектов совпадают параметры id в случае, когда id != null,
     * либо совпадают параметры firstName, lastName, birthday.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof WorkerDb))
            throw new IllegalArgumentException("Parameter object must be instance of "
                    + getClass().getName());

        if (id == null)
            throw new IllegalArgumentException("id is not set!");

        return id.equals(((WorkerDb) obj).getId());
    }

    @Override
    public String toString() {
        return "WorkerDb{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday='" + birthday + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", specialties=" + specialties +
                '}';
    }
}
