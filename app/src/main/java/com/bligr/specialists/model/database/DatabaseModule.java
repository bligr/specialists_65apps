package com.bligr.specialists.model.database;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import com.bligr.specialists.model.database.table.SQLiteTablesModule;
import com.bligr.specialists.model.database.table.SpecialtiesTableHandler;
import com.bligr.specialists.model.database.table.WorkersSpecialtiesTableHandler;
import com.bligr.specialists.model.database.table.WorkersTableHandler;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Igor on 27.09.2017.
 */
@Module(includes = SQLiteTablesModule.class)
public class DatabaseModule {

    private static final int DATABASE_VERSION = 1;

    @Provides
    @Singleton
    IDatabaseConnector provideDatabaseConnector(Context context, WorkersTableHandler workersTable,
                                                SpecialtiesTableHandler specialtiesTable,
                                                WorkersSpecialtiesTableHandler workersSpecialtiesTable) {
        return new DatabaseConnector(context, workersTable, specialtiesTable, workersSpecialtiesTable);
    }

    @Provides
    @Singleton
    SQLiteOpenHelper provideSQLiteOpenHelper(Context context) {
        return new DatabaseProvider(context, "specialists.sqlite", null, DATABASE_VERSION);
    }
}
