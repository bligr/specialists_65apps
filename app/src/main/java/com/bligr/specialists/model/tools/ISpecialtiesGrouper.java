package com.bligr.specialists.model.tools;

import com.bligr.specialists.model.data_manager.entity.Specialty;
import com.bligr.specialists.model.data_manager.entity.Worker;

import java.util.List;

/**
 * Created by Igor on 29.09.2017.
 */
public interface ISpecialtiesGrouper {
    List<Specialty> groupWorkers(List<Worker> workers);
}
