package com.bligr.specialists.model.tools.worker_info_handlers;

/**
 * Created by Igor on 09.10.2017.
 */
public interface IAgesHandler {
    String convertAgeToText(String birthday);
}
