package com.bligr.specialists.model.data_manager;

import com.bligr.specialists.app.AppModule;
import com.bligr.specialists.model.database.DatabaseModule;
import com.bligr.specialists.model.database.table.SQLiteTablesModule;
import com.bligr.specialists.model.mapper.MappersModule;
import com.bligr.specialists.model.server.ServerModule;
import com.bligr.specialists.model.tools.ToolsModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Igor on 26.09.2017.
 */
@Singleton
@Component(modules = {AppModule.class, DatabaseModule.class, SQLiteTablesModule.class,
        ServerModule.class, MappersModule.class, ToolsModule.class})
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
