package com.bligr.specialists.model.mapper;

import com.bligr.specialists.model.tools.worker_info_handlers.INamesHandler;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Igor on 27.09.2017.
 */
@Module
public class MappersModule {

    @Provides
    @Singleton
    SpecialtyMapper provideSpecialtyMapper() {
        return new SpecialtyMapper();
    }

    @Provides
    @Singleton
    WorkersMapper provideWorkersMapper(SpecialtyMapper specialtyMapper,
                                       INamesHandler namesHandler) {
        return new WorkersMapper(specialtyMapper, namesHandler);
    }
}
