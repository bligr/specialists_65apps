package com.bligr.specialists.model.database.entity;

/**
 * Created by Igor on 28.09.2017.
 */
public class SpecialtyDb implements IDatabaseEntity {

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SpecialtyDb{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
