package com.bligr.specialists.model.mapper;

import android.support.annotation.NonNull;

import com.bligr.specialists.model.data_manager.entity.Specialty;
import com.bligr.specialists.model.database.entity.SpecialtyDb;
import com.bligr.specialists.model.server.entity.SpecialtyJson;

/**
 * Created by Igor on 28.09.2017.
 */
public class SpecialtyMapper extends BaseMapper<Specialty, SpecialtyJson, SpecialtyDb> {

    @NonNull
    @Override
    public Specialty toAppEntity(SpecialtyJson jsonEntity) {
        Specialty specialty = new Specialty();
        specialty.setId(jsonEntity.getId());
        specialty.setName(jsonEntity.getName());
        return specialty;
    }

    @NonNull
    @Override
    public Specialty toAppEntity(SpecialtyDb databaseEntity) {
        Specialty specialty = new Specialty();
        specialty.setId(databaseEntity.getId());
        specialty.setName(databaseEntity.getName());
        return specialty;
    }

    @NonNull
    @Override
    public SpecialtyDb toDatabaseEntity(Specialty appEntity) {
        SpecialtyDb specialtyDb = new SpecialtyDb();
        specialtyDb.setId(appEntity.getId());
        specialtyDb.setName(appEntity.getName());
        return specialtyDb;
    }
}
