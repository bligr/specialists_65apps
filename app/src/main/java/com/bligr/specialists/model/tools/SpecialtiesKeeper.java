package com.bligr.specialists.model.tools;

import android.support.annotation.Nullable;

import com.bligr.specialists.model.data_manager.IDataManager;
import com.bligr.specialists.model.data_manager.entity.Specialty;
import com.bligr.specialists.model.data_manager.entity.Worker;

import java.util.List;

import rx.Observable;

/**
 * Created by Igor on 01.10.2017.
 */
class SpecialtiesKeeper implements ISpecialtiesKeeper {

    private final IDataManager dataManager;

//    @Nullable
//    private List<Specialty> specialties;

    SpecialtiesKeeper(IDataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public Observable<List<Specialty>> getAllSpecialties() {
        return Observable.defer(this::getSpecialtiesObservable);
    }

    @Override
    public Observable<List<Worker>> getWorkers(int specialtyId) {
        return Observable.defer(() -> {
            //получаем специальности
            Observable<List<Specialty>> observable = getSpecialtiesObservable();
            //получаем рабочих запрашиваемой специальности
            return observable
                    .map(specialties1 -> getSpecialtyWorkers(specialties1, specialtyId));
        });
    }

    @Nullable
    private List<Worker> getSpecialtyWorkers(List<Specialty> specialties, int specialtyId) {
        for (Specialty specialty : specialties) {
            if (specialty.getId() == specialtyId)
                return specialty.getWorkers();
        }
        return null;
    }

    private Observable<List<Specialty>> getSpecialtiesObservable() {
//        if (specialties == null)
            return loadSpecialties();
//        return Observable.just(specialties);
    }

    private Observable<List<Specialty>> loadSpecialties() {
        return dataManager.loadSpecialities()
//                .doOnNext(specialties1 -> this.specialties = specialties1)
                ;
    }
}