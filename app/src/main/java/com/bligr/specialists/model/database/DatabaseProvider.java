package com.bligr.specialists.model.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Igor on 29.09.2017.
 */
class DatabaseProvider extends SQLiteOpenHelper {

    DatabaseProvider(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //создание таблицы с рабочими
        sqLiteDatabase.execSQL("CREATE TABLE " + DatabaseConstants.TABLE_WORKERS + " (" +
                " \"" + DatabaseConstants.COLUMN_WORKER_ID + "\" INTEGER PRIMARY KEY," +
                "  \"" + DatabaseConstants.COLUMN_FIRST_NAME + "\" TEXT," +
                "  \"" + DatabaseConstants.COLUMN_LAST_NAME + "\" TEXT," +
                "  \"" + DatabaseConstants.COLUMN_BIRTHDAY + "\" TEXT," +
                "  \"" + DatabaseConstants.COLUMN_AVATAR_URL + "\" TEXT" +
                ")");
        //создание таблицы со специальностями
        sqLiteDatabase.execSQL("CREATE TABLE " + DatabaseConstants.TABLE_SPECIALTIES + " (" +
                "  \"" + DatabaseConstants.COLUMN_SPECIALTY_ID + "\" INTEGER PRIMARY KEY," +
                "  \"" + DatabaseConstants.COLUMN_NAME + "\" TEXT" +
                ")");
        //создание связывающей таблицы для рабочих и специальностей
        sqLiteDatabase.execSQL("CREATE TABLE " + DatabaseConstants.TABLE_WORKERS_SPECIALTIES + " (" +
                "  \"" + DatabaseConstants.COLUMN_ID + "\" INTEGER PRIMARY KEY AUTOINCREMENT," +
                "  \"" + DatabaseConstants.COLUMN_WORKER_ID + "\" INTEGER," +
                "  \"" + DatabaseConstants.COLUMN_SPECIALTY_ID + "\" INTEGER" +
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }
}
