package com.bligr.specialists.model.tools.worker_info_handlers;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Igor on 27.09.2017.
 */
@Module
public class WorkerInfoHandlersModule {

    @Provides
    @Singleton
    IAgeCalculator provideAgeCalculator() {
        return new AgeCalculator();
    }

    @Provides
    @Singleton
    INamesHandler provideNamesHandler(Context context) {
        return new NamesHandler(context);
    }

    @Provides
    @Singleton
    IAgesHandler provideAgesHandler(Context context, IAgeCalculator ageCalculator) {
        return new AgesHandler(context, ageCalculator);
    }
}
