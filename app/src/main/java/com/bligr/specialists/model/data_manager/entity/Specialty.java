package com.bligr.specialists.model.data_manager.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Igor on 23.09.2017.
 */
public class Specialty implements IAppEntity, Serializable {

    private int id;
    private String name;
    private List<Worker> workers;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(List<Worker> workers) {
        this.workers = workers;
    }

    public void addWorker(Worker worker) {
        if (workers == null)
            workers = new LinkedList<>();
        workers.add(worker);
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Specialty))
            throw new IllegalArgumentException("Parameter object must be instance of "
                    + getClass().getName());
        return id == ((Specialty) object).getId();
    }
}
