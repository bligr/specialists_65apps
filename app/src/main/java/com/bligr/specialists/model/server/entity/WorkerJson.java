package com.bligr.specialists.model.server.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Igor on 28.09.2017.
 */
public class WorkerJson implements IJsonEntity {

    @SerializedName("f_name")
    @Expose
    private String firstName;

    @SerializedName("l_name")
    @Expose
    private String lastName;

    @SerializedName("birthday")
    @Expose
    private String birthday;

    @SerializedName("avatr_url")
    @Expose
    private String avatarUrl;

    @SerializedName("specialty")
    @Expose
    private List<SpecialtyJson> specialties;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public List<SpecialtyJson> getSpecialties() {
        return specialties;
    }

    public void setSpecialties(List<SpecialtyJson> specialties) {
        this.specialties = specialties;
    }
}
