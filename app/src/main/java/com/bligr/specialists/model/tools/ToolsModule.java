package com.bligr.specialists.model.tools;

import com.bligr.specialists.model.data_manager.IDataManager;
import com.bligr.specialists.model.tools.image_loaders.ImageLoadersModule;
import com.bligr.specialists.model.tools.worker_info_handlers.WorkerInfoHandlersModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Igor on 27.09.2017.
 */
@Module(includes = {ImageLoadersModule.class, WorkerInfoHandlersModule.class})
public class ToolsModule {

    @Provides
    @Singleton
    ISpecialtiesGrouper provideSpecialtiesGrouper() {
        return new SpecialtiesGrouper();
    }

    @Provides
    @Singleton
    ISpecialtiesKeeper provideSpecialtiesKeeper(IDataManager dataManager) {
        return new SpecialtiesKeeper(dataManager);
    }
}
