package com.bligr.specialists.model.server;

import android.content.Context;

import com.bligr.specialists.BuildConfig;
import com.bligr.specialists.R;
import com.bligr.specialists.model.server.service.ITestTaskService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Igor on 27.09.2017.
 */
@Module
public class ServerModule {

    private static final int CONNECT_TIMEOUT_SECONDS = 10;
    private static final int IO_TIMEOUT_SECONDS = 30;

    @Provides
    @Singleton
    IServerConnector provideServerConnector(ITestTaskService testTaskService) {
        return new ServerConnector(testTaskService);
    }

    @Provides
    @Singleton
    ITestTaskService provideTestTaskService(Context context) {
        return createTestTaskService(context);
    }

    private ITestTaskService createTestTaskService(Context context) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);
        }

        OkHttpClient okHttpClient = builder
                .connectTimeout(CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .readTimeout(IO_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(IO_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .baseUrl(context.getString(R.string.base_url))
                .client(okHttpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ITestTaskService.class);
    }
}
