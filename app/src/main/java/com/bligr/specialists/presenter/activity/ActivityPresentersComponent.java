package com.bligr.specialists.presenter.activity;

import com.bligr.specialists.app.AppModule;
import com.bligr.specialists.model.data_manager.DataManagerModule;
import com.bligr.specialists.model.tools.ToolsModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Igor on 26.09.2017.
 */
@Singleton
@Component(modules = {AppModule.class, DataManagerModule.class, ToolsModule.class})
public interface ActivityPresentersComponent {
    void inject(MainActivityPresenter presenter);

    void inject(WorkersActivityPresenter presenter);

    void inject(WorkerInfoActivityPresenter presenter);
}
