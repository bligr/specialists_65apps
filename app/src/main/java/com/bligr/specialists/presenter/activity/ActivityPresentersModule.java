package com.bligr.specialists.presenter.activity;

import com.bligr.specialists.model.tools.ToolsModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Igor on 27.09.2017.
 */
@Singleton
@Module(includes = ToolsModule.class)
public class ActivityPresentersModule {

    @Provides
    IMainActivityPresenter provideMainActivityPresenter() {
        return new MainActivityPresenter();
    }

    @Provides
    IWorkersActivityPresenter provideWorkersActivityPresenter() {
        return new WorkersActivityPresenter();
    }

    @Provides
    IWorkerInfoActivityPresenter provideWorkerInfoActivityPresenter() {
        return new WorkerInfoActivityPresenter();
    }
}
