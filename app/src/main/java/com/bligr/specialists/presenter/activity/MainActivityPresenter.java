package com.bligr.specialists.presenter.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.bligr.specialists.model.data_manager.entity.Specialty;
import com.bligr.specialists.model.tools.ISpecialtiesKeeper;
import com.bligr.specialists.presenter.BasePresenter;
import com.bligr.specialists.view.activity.IMainActivityView;
import com.bligr.specialists.view.activity.WorkersActivity;
import com.bligr.specialists.view.recycler_view.adapter.SpecialitiesAdapter;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import rx.Subscription;

/**
 * Created by Igor on 23.09.2017.
 */
class MainActivityPresenter extends BasePresenter<IMainActivityView>
        implements IMainActivityPresenter {

    private static final int RECYCLER_VIEW_ORIENTATION = LinearLayoutManager.VERTICAL;

    @Inject ISpecialtiesKeeper specialtiesKeeper;

    @NonNull
    private final List<Specialty> specialities = new LinkedList<>();
    @NonNull
    private final SpecialitiesAdapter adapter = new SpecialitiesAdapter(specialities);

    MainActivityPresenter() {
        super();
        adapter.setOnSpecialityClickListener(this::startWorkersActivity);
    }

    @Override
    protected void injectDagger(ActivityPresentersComponent activityPresentersComponent) {
        activityPresentersComponent.inject(this);
    }

    @Override
    public void loadSpecialities(boolean pullToRefresh) {
        Subscription subscription = specialtiesKeeper.getAllSpecialties()
                .doOnSubscribe(() -> mvpView.showLoading(pullToRefresh))
                .subscribe(specialities -> {
                    mvpView.setData(specialities);
                    mvpView.showContent();
                }, throwable -> {
                    throwable.printStackTrace();
                    mvpView.showError(throwable, pullToRefresh);
                });
        addSubscription(subscription);
    }

    @Override
    public RecyclerView.ItemDecoration getDividerItemDecoration() {
        return new DividerItemDecoration(mvpView.getActivityContext(), RECYCLER_VIEW_ORIENTATION);
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mvpView.getActivityContext());
        layoutManager.setOrientation(RECYCLER_VIEW_ORIENTATION);
        return layoutManager;
    }

    @Override
    public RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    @Override
    public void updateSpecialities(List<Specialty> specialties) {
        if (this.specialities != specialties) {
            this.specialities.clear();
            this.specialities.addAll(specialties);
            this.adapter.notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public List<Specialty> getSpecialities() {
        return specialities;
    }

    private void startWorkersActivity(Specialty specialty) {
        Intent intent = new Intent(mvpView.getActivityContext(), WorkersActivity.class);
        intent.putExtra(WorkersActivity.SPECIALTY_ID, specialty.getId());
        intent.putExtra(WorkersActivity.SPECIALTY_NAME, specialty.getName());
        mvpView.startActivity(intent);
    }
}
