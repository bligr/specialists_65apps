package com.bligr.specialists.presenter.activity;

import android.support.v7.widget.RecyclerView;

import com.bligr.specialists.model.data_manager.entity.Specialty;
import com.bligr.specialists.view.activity.IMainActivityView;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by Igor on 23.09.2017.
 */
public interface IMainActivityPresenter extends MvpPresenter<IMainActivityView> {

    void loadSpecialities(boolean pullToRefresh);

    RecyclerView.ItemDecoration getDividerItemDecoration();

    RecyclerView.LayoutManager getLayoutManager();

    RecyclerView.Adapter getAdapter();

    void updateSpecialities(List<Specialty> specialties);

    List<Specialty> getSpecialities();
}
