package com.bligr.specialists.presenter.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.bligr.specialists.model.data_manager.entity.Worker;
import com.bligr.specialists.model.tools.ISpecialtiesKeeper;
import com.bligr.specialists.presenter.BasePresenter;
import com.bligr.specialists.view.activity.IWorkersActivityView;
import com.bligr.specialists.view.activity.WorkerInfoActivity;
import com.bligr.specialists.view.recycler_view.adapter.WorkersAdapter;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import rx.Subscription;

/**
 * Created by Igor on 02.10.2017.
 */
class WorkersActivityPresenter  extends BasePresenter<IWorkersActivityView>
        implements IWorkersActivityPresenter {

    private static final int RECYCLER_VIEW_ORIENTATION = LinearLayoutManager.VERTICAL;

    @Inject ISpecialtiesKeeper specialtiesKeeper;

    @NonNull
    private final List<Worker> workers = new LinkedList<>();
    @NonNull
    private final WorkersAdapter adapter = new WorkersAdapter(workers);

    WorkersActivityPresenter() {
        super();
        adapter.setOnWorkerClickListener(this::startWorkerInfoActivity);
    }

    @Override
    protected void injectDagger(ActivityPresentersComponent activityPresentersComponent) {
        activityPresentersComponent.inject(this);
    }

    @Override
    public void loadWorkers(boolean pullToRefresh, int specialtyId) {
        Subscription subscription = specialtiesKeeper.getWorkers(specialtyId)
                .doOnSubscribe(() -> mvpView.showLoading(pullToRefresh))
                .subscribe(workers -> {
                    mvpView.setData(workers);
                    mvpView.showContent();
                }, throwable -> {
                    throwable.printStackTrace();
                    mvpView.showError(throwable, pullToRefresh);
                });
        addSubscription(subscription);
    }

    @Override
    public void updateWorkers(List<Worker> workers) {
        if (this.workers != workers) {
            this.workers.clear();
            this.workers.addAll(workers);
            this.adapter.notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public List<Worker> getWorkers() {
        return workers;
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mvpView.getActivityContext());
        layoutManager.setOrientation(RECYCLER_VIEW_ORIENTATION);
        return layoutManager;
    }

    @Override
    public RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    private void startWorkerInfoActivity(Worker worker) {
        Intent intent = new Intent(mvpView.getActivityContext(), WorkerInfoActivity.class);
        intent.putExtra(WorkerInfoActivity.WORKER, worker);
        mvpView.startActivity(intent);

    }
}
