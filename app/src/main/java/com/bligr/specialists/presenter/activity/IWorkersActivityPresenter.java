package com.bligr.specialists.presenter.activity;

import android.support.v7.widget.RecyclerView;

import com.bligr.specialists.model.data_manager.entity.Worker;
import com.bligr.specialists.view.activity.IWorkersActivityView;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.List;

/**
 * Created by Igor on 23.09.2017.
 */
public interface IWorkersActivityPresenter extends MvpPresenter<IWorkersActivityView> {
    void loadWorkers(boolean pullToRefresh, int specialtyId);

    void updateWorkers(List<Worker> workers);

    List<Worker> getWorkers();

    RecyclerView.LayoutManager getLayoutManager();

    RecyclerView.Adapter getAdapter();
}
