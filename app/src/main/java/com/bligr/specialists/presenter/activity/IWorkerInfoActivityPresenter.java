package com.bligr.specialists.presenter.activity;

import com.bligr.specialists.view.activity.IWorkerInfoActivityView;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.io.Serializable;

/**
 * Created by Igor on 23.09.2017.
 */
public interface IWorkerInfoActivityPresenter extends MvpPresenter<IWorkerInfoActivityView> {
    void loadWorkerFromIntent(Serializable workerExtra);
}
