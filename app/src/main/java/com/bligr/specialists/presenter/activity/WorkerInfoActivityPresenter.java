package com.bligr.specialists.presenter.activity;

import com.bligr.specialists.model.data_manager.entity.Worker;
import com.bligr.specialists.presenter.BasePresenter;
import com.bligr.specialists.view.activity.IWorkerInfoActivityView;

import java.io.Serializable;

/**
 * Created by Igor on 02.10.2017.
 */
class WorkerInfoActivityPresenter
        extends BasePresenter<IWorkerInfoActivityView>
        implements IWorkerInfoActivityPresenter {

    WorkerInfoActivityPresenter() {
    }

    @Override
    protected void injectDagger(ActivityPresentersComponent activityPresentersComponent) {
        activityPresentersComponent.inject(this);
    }

    @Override
    public void loadWorkerFromIntent(Serializable workerExtra) {
        if (workerExtra == null || !(workerExtra instanceof Worker)) {
            mvpView.showError(new IllegalArgumentException("Worker not set in intent!"), false);
        } else {
            Worker worker = (Worker) workerExtra;
            mvpView.setData(worker);
            mvpView.showContent();
        }
    }
}
