package com.bligr.specialists.presenter;

import com.bligr.specialists.app.DaggerComponents;
import com.bligr.specialists.presenter.activity.ActivityPresentersComponent;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Igor on 23.09.2017.
 */
public abstract class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    protected V mvpView;

    private final CompositeSubscription compositeSubscription = new CompositeSubscription();

    public BasePresenter() {
        injectDagger(DaggerComponents.getActivityPresentersComponent());
    }

    protected abstract void injectDagger(ActivityPresentersComponent activityPresentersComponent);

    @Override
    public void attachView(V view) {
        this.mvpView = view;
    }

    @Override
    public void detachView(boolean retainInstance) {
        this.mvpView = null;
        clearSubscriptions();
    }

    protected boolean isViewAttached() {
        return this.mvpView != null;
    }

    protected void addSubscription(Subscription subscription) {
        compositeSubscription.add(subscription);
    }

    private void clearSubscriptions() {
        if (compositeSubscription.hasSubscriptions())
            compositeSubscription.unsubscribe();
    }
}
