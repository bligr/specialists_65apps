package com.bligr.specialists.view.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;

import com.bligr.specialists.R;
import com.bligr.specialists.app.DaggerComponents;
import com.bligr.specialists.model.data_manager.entity.Specialty;
import com.bligr.specialists.presenter.activity.IMainActivityPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity
        extends BaseMvpLceViewStateActivity<RecyclerView, List<Specialty>, IMainActivityView, IMainActivityPresenter>
        implements IMainActivityView, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;

    public MainActivity() {
        DaggerComponents.getActivitiesComponent().inject(this);
    }

    @Override
    void injectDagger(ActivitiesComponent activitiesComponent) {
        activitiesComponent.inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setRestoringViewState(true);

        swipeRefreshLayout.setOnRefreshListener(this);

        initRecyclerView();
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.loadSpecialities(pullToRefresh);
    }

    @Override
    public void setData(List<Specialty> data) {
        presenter.updateSpecialities(data);
    }

    @Override
    public void showContent() {
        super.showContent();
        stopRefreshing();
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        stopRefreshing();
    }

    @Override
    public List<Specialty> getData() {
        return presenter.getSpecialities();
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getString(R.string.error_on_loading);
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    private void initRecyclerView() {
        contentView.setLayoutManager(presenter.getLayoutManager());
//        contentView.addItemDecoration(presenter.getDividerItemDecoration());
        contentView.setAdapter(presenter.getAdapter());
    }

    private void stopRefreshing() {
        swipeRefreshLayout.setRefreshing(false);
    }
}
