package com.bligr.specialists.view.recycler_view.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bligr.specialists.R;
import com.bligr.specialists.model.data_manager.entity.Worker;
import com.bligr.specialists.view.recycler_view.view_holder.WorkerViewHolder;

import java.util.List;

import rx.functions.Action1;

/**
 * Created by Igor on 23.09.2017.
 */
public class WorkersAdapter extends RecyclerView.Adapter<WorkerViewHolder> {

    private final List<Worker> workers;

    @Nullable
    private Action1<Worker> onWorkerClickListener;

    public WorkersAdapter(List<Worker> workers) {
        this.workers = workers;
    }

    @Override
    public WorkerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_worker, parent, false);
        return new WorkerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WorkerViewHolder holder, int position) {
        Worker worker = workers.get(position);
        holder.configure(worker);
        holder.setOnClickListener(view -> {
            if (onWorkerClickListener != null) {
                onWorkerClickListener.call(worker);
            }
        });
    }

    @Override
    public int getItemCount() {
        return workers.size();
    }

    public void setOnWorkerClickListener(@Nullable Action1<Worker> onWorkerClickListener) {
        this.onWorkerClickListener = onWorkerClickListener;
    }
}
