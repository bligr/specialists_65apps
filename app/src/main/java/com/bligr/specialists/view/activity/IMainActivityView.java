package com.bligr.specialists.view.activity;

import com.bligr.specialists.model.data_manager.entity.Specialty;

import java.util.List;

/**
 * Created by Igor on 23.09.2017.
 */
public interface IMainActivityView extends IBaseMvpLceView<List<Specialty>> {
}
