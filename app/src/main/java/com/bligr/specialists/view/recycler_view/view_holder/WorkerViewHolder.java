package com.bligr.specialists.view.recycler_view.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bligr.specialists.R;
import com.bligr.specialists.app.DaggerComponents;
import com.bligr.specialists.model.data_manager.entity.Worker;
import com.bligr.specialists.model.tools.image_loaders.IImageLoader;
import com.bligr.specialists.model.tools.worker_info_handlers.IAgesHandler;
import com.bligr.specialists.model.tools.worker_info_handlers.INamesHandler;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Igor on 23.09.2017.
 */
@SuppressWarnings("WeakerAccess")
public class WorkerViewHolder extends RecyclerView.ViewHolder {

    @Inject IImageLoader imageLoader;
    @Inject IAgesHandler agesHandler;
    @Inject INamesHandler namesHandler;

    @BindView(R.id.name) TextView tvName;
    @BindView(R.id.age) TextView tvAge;
    @BindView(R.id.avatar) ImageView ivAvatar;

    public WorkerViewHolder(View itemView) {
        super(itemView);

        DaggerComponents.getRecyclerViewsComponent().inject(this);
        ButterKnife.bind(this, itemView);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        itemView.setOnClickListener(onClickListener);
    }

    public void configure(Worker worker) {
        setWorkerName(worker.getFirstName(), worker.getLastName());
        setAge(worker.getBirthday());
        loadAvatar(worker.getAvatarUrl());
    }

    public void setWorkerName(String firstName, String lastName) {
        String workerName = namesHandler.toFullName(firstName, lastName);
        tvName.setText(workerName);
    }

    public void loadAvatar(String avatarUrl) {
        imageLoader.loadImageUrl(avatarUrl, ivAvatar);
    }

    public void setAge(String birthday) {
        String workerAgeStr = agesHandler.convertAgeToText(birthday);
        tvAge.setText(workerAgeStr);
    }
}
