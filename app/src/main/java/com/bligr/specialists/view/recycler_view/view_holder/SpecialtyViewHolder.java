package com.bligr.specialists.view.recycler_view.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bligr.specialists.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Igor on 23.09.2017.
 */
public class SpecialtyViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.title) TextView tvTitle;

    public SpecialtyViewHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);
    }

    public void setTitleText(String text) {
        tvTitle.setText(text);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        itemView.setOnClickListener(onClickListener);
    }
}
