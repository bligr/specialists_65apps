package com.bligr.specialists.view.activity;

import com.bligr.specialists.model.data_manager.entity.Worker;

/**
 * Created by Igor on 02.10.2017.
 */
public interface IWorkerInfoActivityView extends IBaseMvpLceView<Worker> {
}
