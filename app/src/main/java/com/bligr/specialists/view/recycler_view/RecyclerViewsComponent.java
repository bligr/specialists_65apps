package com.bligr.specialists.view.recycler_view;

import com.bligr.specialists.app.AppModule;
import com.bligr.specialists.model.tools.ToolsModule;
import com.bligr.specialists.view.recycler_view.view_holder.WorkerViewHolder;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Igor on 26.09.2017.
 */
@Singleton
@Component(modules = {AppModule.class, ToolsModule.class})
public interface RecyclerViewsComponent {
    void inject(WorkerViewHolder viewHolder);
}
