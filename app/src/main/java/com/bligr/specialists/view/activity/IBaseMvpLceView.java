package com.bligr.specialists.view.activity;

import android.content.Context;
import android.content.Intent;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

/**
 * Created by Igor on 02.10.2017.
 */
interface IBaseMvpLceView<M> extends MvpLceView<M> {
    Context getActivityContext();

    void startActivity(Intent intent);
}
