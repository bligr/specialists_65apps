package com.bligr.specialists.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.bligr.specialists.R;
import com.bligr.specialists.model.data_manager.entity.Worker;
import com.bligr.specialists.presenter.activity.IWorkersActivityPresenter;

import java.util.List;

public class WorkersActivity
        extends BaseMvpLceViewStateActivity<RecyclerView, List<Worker>, IWorkersActivityView, IWorkersActivityPresenter>
        implements IWorkersActivityView {

    public static final String SPECIALTY_ID = "specialty_id";
    public static final String SPECIALTY_NAME = "specialty_name";
    public static final int ILLEGAL_SPECIALTY_ID = -1;

    @Override
    void injectDagger(ActivitiesComponent activitiesComponent) {
        activitiesComponent.inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workers);

        setRestoringViewState(true);

        initTitle();
        initRecyclerView();
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        Intent intent = getIntent();
        int specialtyId = intent.getIntExtra(SPECIALTY_ID, ILLEGAL_SPECIALTY_ID);
        presenter.loadWorkers(pullToRefresh, specialtyId);
    }

    @Override
    public void setData(List<Worker> data) {
        presenter.updateWorkers(data);
    }

    @Override
    public List<Worker> getData() {
        return presenter.getWorkers();
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getString(R.string.error_on_loading);
    }

    private void initTitle() {
        Intent intent = getIntent();
        String name = intent.getStringExtra(SPECIALTY_NAME);
        if (name != null)
            setTitle(name);
    }

    private void initRecyclerView() {
        contentView.setLayoutManager(presenter.getLayoutManager());
//        contentView.addItemDecoration(presenter.getDividerItemDecoration());
        contentView.setAdapter(presenter.getAdapter());
    }

}
