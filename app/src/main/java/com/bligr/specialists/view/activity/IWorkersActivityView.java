package com.bligr.specialists.view.activity;

import com.bligr.specialists.model.data_manager.entity.Worker;

import java.util.List;

/**
 * Created by Igor on 02.10.2017.
 */
public interface IWorkersActivityView extends IBaseMvpLceView<List<Worker>> {
}
