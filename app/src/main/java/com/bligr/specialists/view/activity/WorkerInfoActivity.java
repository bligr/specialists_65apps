package com.bligr.specialists.view.activity;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bligr.specialists.R;
import com.bligr.specialists.model.data_manager.entity.Specialty;
import com.bligr.specialists.model.data_manager.entity.Worker;
import com.bligr.specialists.model.tools.image_loaders.IImageLoader;
import com.bligr.specialists.model.tools.worker_info_handlers.IAgesHandler;
import com.bligr.specialists.model.tools.worker_info_handlers.INamesHandler;
import com.bligr.specialists.presenter.activity.IWorkerInfoActivityPresenter;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Igor on 02.10.2017.
 */
public class WorkerInfoActivity
        extends BaseMvpLceViewStateActivity<ViewGroup, Worker, IWorkerInfoActivityView, IWorkerInfoActivityPresenter>
        implements IWorkerInfoActivityView {

    public static final String WORKER = "worker";
    private static final String DIVIDER = ", ";

    @Inject IImageLoader imageLoader;
    @Inject INamesHandler namesHandler;
    @Inject IAgesHandler agesHandler;

    @BindView(R.id.avatar) ImageView ivAvatar;
    @BindView(R.id.first_name) TextView tvFirstName;
    @BindView(R.id.last_name) TextView tvLastName;
    @BindView(R.id.age) TextView tvAge;
    @BindView(R.id.birthday) TextView tvBirthday;
    @BindView(R.id.specialties) TextView tvSpecialties;

    private Worker worker;

    @Override
    void injectDagger(ActivitiesComponent activitiesComponent) {
        activitiesComponent.inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_info);

        ButterKnife.bind(this);

        setRestoringViewState(true);
    }

    @Override
    public Worker getData() {
        return worker;
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getString(R.string.error_on_loading);
    }

    @Override
    public void setData(Worker worker) {
        this.worker = worker;
    }

    @Override
    public void showContent() {
        super.showContent();
        showTitle();
        showAvatar();
        showFirstName();
        showLastName();
        showAge();
        showBirthday();
        showSpecialties();
    }

    private void showTitle() {
        String fullName = namesHandler.toFullName(worker);
        if (fullName != null)
            setTitle(fullName);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        Serializable workerExtra = getIntent().getSerializableExtra(WORKER);
        presenter.loadWorkerFromIntent(workerExtra);
    }

    private void showAvatar() {
        imageLoader.loadImageUrl(worker.getAvatarUrl(), ivAvatar);
    }

    private void showFirstName() {
        tvFirstName.setText(worker.getFirstName());
    }

    private void showLastName() {
        tvLastName.setText(worker.getLastName());
    }

    private void showBirthday() {
        String birthday = worker.getBirthday();
        String placeholder = getString(R.string.no_data_placeholder);
        tvBirthday.setText(birthday == null || birthday.isEmpty() ? placeholder : birthday);
    }

    private void showSpecialties() {
        List<Specialty> specialties = worker.getSpecialties();
        String specialtiesStr = "";
        if (specialties != null) {
            for (int i = 0; i < specialties.size(); i++) {
                Specialty specialty = specialties.get(i);
                specialtiesStr += specialty.getName();
                if (i < specialties.size() - 1)
                    specialtiesStr += DIVIDER;
            }
        } else {
            specialtiesStr = getString(R.string.no_data_placeholder);
        }
        tvSpecialties.setText(specialtiesStr);
    }

    public void showAge() {
        String birthday = worker.getBirthday();
        String workerAgeStr = agesHandler.convertAgeToText(birthday);
        tvAge.setText(workerAgeStr);
    }
}
