package com.bligr.specialists.view.activity;

import com.bligr.specialists.app.AppModule;
import com.bligr.specialists.presenter.activity.ActivityPresentersModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Igor on 26.09.2017.
 */
@Singleton
@Component(modules = {AppModule.class, ActivityPresentersModule.class})
public interface ActivitiesComponent {
    void inject(MainActivity activity);
    void inject(WorkersActivity activity);
    void inject(WorkerInfoActivity activity);
}
