package com.bligr.specialists.view.recycler_view.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bligr.specialists.R;
import com.bligr.specialists.model.data_manager.entity.Specialty;
import com.bligr.specialists.view.recycler_view.view_holder.SpecialtyViewHolder;

import java.util.List;

import rx.functions.Action1;

/**
 * Created by Igor on 23.09.2017.
 */
public class SpecialitiesAdapter extends RecyclerView.Adapter<SpecialtyViewHolder> {

    private final List<Specialty> specialities;

    @Nullable
    private Action1<Specialty> onSpecialityClickListener;

    public SpecialitiesAdapter(List<Specialty> specialities) {
        this.specialities = specialities;
    }

    @Override
    public SpecialtyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_speciality, parent, false);
        return new SpecialtyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SpecialtyViewHolder holder, int position) {
        Specialty specialty = specialities.get(position);
        String name = specialty.getName();
        holder.setTitleText(name);
        holder.setOnClickListener(view -> {
            if (onSpecialityClickListener != null) {
                onSpecialityClickListener.call(specialty);
            }
        });
    }

    @Override
    public int getItemCount() {
        return specialities.size();
    }

    public void setOnSpecialityClickListener(@Nullable Action1<Specialty> onSpecialityClickListener) {
        this.onSpecialityClickListener = onSpecialityClickListener;
    }
}
