package com.bligr.specialists.view.activity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

import com.bligr.specialists.app.DaggerComponents;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.MvpLceViewStateActivity;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.data.RetainingLceViewState;

import javax.inject.Inject;

/**
 * Created by Igor on 02.10.2017.
 */
public abstract class BaseMvpLceViewStateActivity<CV extends View, M, V extends IBaseMvpLceView<M>, P extends MvpPresenter<V>>
        extends MvpLceViewStateActivity<CV, M, V, P>
        implements IBaseMvpLceView<M> {

    @Inject protected P injectedPresenter;

    public BaseMvpLceViewStateActivity() {
        injectDagger(DaggerComponents.getActivitiesComponent());
    }

    abstract void injectDagger(ActivitiesComponent activitiesComponent);

    @NonNull
    @Override
    public final P createPresenter() {
        return injectedPresenter;
    }

    @NonNull
    @Override
    public final LceViewState<M, V> createViewState() {
        return new RetainingLceViewState<>();
    }

    @Override
    public void showContent() {
        //показываем без анимации
        contentView.setVisibility(View.VISIBLE);
        loadingView.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        //из super-класса
        viewState.setStateShowContent(getData());
    }

    @Override
    public final Context getActivityContext() {
        return this;
    }
}
