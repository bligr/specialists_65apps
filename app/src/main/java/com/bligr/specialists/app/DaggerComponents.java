package com.bligr.specialists.app;

import android.content.Context;

import com.bligr.specialists.model.data_manager.DaggerDataManagerComponent;
import com.bligr.specialists.model.data_manager.DataManagerComponent;
import com.bligr.specialists.model.data_manager.DataManagerModule;
import com.bligr.specialists.model.database.DatabaseModule;
import com.bligr.specialists.model.server.ServerModule;
import com.bligr.specialists.model.tools.ToolsModule;
import com.bligr.specialists.presenter.activity.ActivityPresentersComponent;
import com.bligr.specialists.presenter.activity.ActivityPresentersModule;
import com.bligr.specialists.presenter.activity.DaggerActivityPresentersComponent;
import com.bligr.specialists.view.activity.ActivitiesComponent;
import com.bligr.specialists.view.activity.DaggerActivitiesComponent;
import com.bligr.specialists.view.recycler_view.DaggerRecyclerViewsComponent;
import com.bligr.specialists.view.recycler_view.RecyclerViewsComponent;

/**
 * Created by Igor on 26.09.2017.
 */
public class DaggerComponents {

    private static ActivitiesComponent activitiesComponent;
    private static RecyclerViewsComponent recyclerViewsComponent;
    private static ActivityPresentersComponent activityPresentersComponent;
    private static DataManagerComponent dataManagerComponent;

    static void init(Context applicationContext) {
        AppModule appModule = new AppModule(applicationContext);
        ActivityPresentersModule activityPresentersModule = new ActivityPresentersModule();
        DatabaseModule databaseModule = new DatabaseModule();
        ServerModule serverModule = new ServerModule();
        ToolsModule toolsModule = new ToolsModule();
        DataManagerModule dataManagerModule = new DataManagerModule();

        activitiesComponent = DaggerActivitiesComponent.builder()
                .appModule(appModule)
                .activityPresentersModule(activityPresentersModule)
                .toolsModule(toolsModule)
                .build();

        recyclerViewsComponent = DaggerRecyclerViewsComponent.builder()
                .appModule(appModule)
                .toolsModule(toolsModule)
                .build();

        activityPresentersComponent = DaggerActivityPresentersComponent.builder()
                .appModule(appModule)
                .dataManagerModule(dataManagerModule)
                .build();

        dataManagerComponent = DaggerDataManagerComponent.builder()
                .appModule(appModule)
                .databaseModule(databaseModule)
                .serverModule(serverModule)
                .toolsModule(toolsModule)
                .build();
    }

    public static ActivitiesComponent getActivitiesComponent() {
        return activitiesComponent;
    }

    public static RecyclerViewsComponent getRecyclerViewsComponent() {
        return recyclerViewsComponent;
    }

    public static ActivityPresentersComponent getActivityPresentersComponent() {
        return activityPresentersComponent;
    }

    public static DataManagerComponent getDataManagerComponent() {
        return dataManagerComponent;
    }
}
