package com.bligr.specialists.app;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Igor on 26.09.2017.
 */
@Module
public class AppModule {

    private final Context applicationContext;

    AppModule(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return applicationContext;
    }
}
