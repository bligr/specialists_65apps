package com.bligr.specialists.app;

import android.app.Application;

/**
 * Created by Igor on 26.09.2017.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        DaggerComponents.init(getApplicationContext());
    }
}
